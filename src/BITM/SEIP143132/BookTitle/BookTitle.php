<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;


class BookTitle extends DB{
    public $id;
    public $booktitle;
    public $author_name;
    public function __construct(){

        parent::__construct();
    }
    public function index(){
        echo "I'm inside the index of BookTitle Class";
    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('booktitle',$data)){
            $this->booktitle=$data['booktitle'];
        }
        if(array_key_exists('author_name',$data)){
            $this->author_name=$data['author_name'];
        }
    }
    public function store(){
        $arrData = array($this->booktitle,$this->author_name);
        $sql="insert into book_title(booktitle, author_name) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        utility::redirect('create.php');

    }

}
